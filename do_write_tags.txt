
[Numeric.0.0]

Name	Value	Extent	TreatAs	Persist	ScaleTo	Sim	OnWrite	HasSP	Label	Alias	Desc	Class	FormType	LimitMin	LimitMax	LimitType	Deadband	ColType	Event1 / Mode	Event2 / Mode	Trigger1 / Mode	Trigger2 / Mode	QuickPlot / Mode	Sec / Access	Sec / Logging
DO_write.AirPressure_unit		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.AirPressure_value		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Autoclavings		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.CP1_DO_value		2	Floating Point	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.CP2_DO_value		2	Floating Point	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.CP6_commands		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.CP6_DO_value		2	Floating Point	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Meas_interval_unit		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Meas_interval_value		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Min_sub_meas_auto_unit		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Min_sub_meas_auto_value		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.MovingAvg_unit		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.MovingAvg_value		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Operator_level_w		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Password_w		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.PMC1_unit		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.PMC6_unit		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Resolution_unit		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Resolution_value		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Salinity_unit		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Salinity_value		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Sensor_cap_Unit		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Sensor_cap_value		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Std_cal_stability_O2_w		2	Floating Point	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.Std_cal_stability_Temp_w		2	Floating Point	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_write.system_time_counter		2	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
