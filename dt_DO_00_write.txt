
[Numeric.0.0]

Name	Value	Extent	TreatAs	Persist	ScaleTo	Sim	OnWrite	HasSP	Label	Alias	Desc	Class	FormType	LimitMin	LimitMax	LimitType	Deadband	ColType	Event1 / Mode	Event2 / Mode	Trigger1 / Mode	Trigger2 / Mode	QuickPlot / Mode	Sec / Access	Sec / Logging
DO_00_write.AirPressure_unit		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.AirPressure_value		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Autoclavings		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.CP1_DO_value		0	Floating Point	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.CP2_DO_value		0	Floating Point	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.CP6_commands		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.CP6_DO_value		0	Floating Point	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Meas_interval_unit		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Meas_interval_value		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Min_sub_meas_auto_unit		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Min_sub_meas_auto_value		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.MovingAvg_unit		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.MovingAvg_value		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Operator_level_w		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Password_w		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.PMC1_unit		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.PMC6_unit		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Resolution_unit		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Resolution_value		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Salinity_unit		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Salinity_value		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Sensor_cap_Unit		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Sensor_cap_value		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Std_cal_stability_O2_w		0	Floating Point	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.Std_cal_stability_Temp_w		0	Floating Point	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
DO_00_write.system_time_counter		0	Signed Integer	Non-Retentive	Do Not Scale			No					General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
