PLC1.C11:0000:BITS=0
DO_read.PMC1_value_raw[1]
PLC1.C11:0001:BITS=0
DO_read.PMC1_min_raw[1]
PLC1.C11:0002:BITS=0
DO_read.PMC1_max_raw[1]
PLC1.C11:0003:BITS=0
DO_read.PMC6_value_raw[1]
PLC1.C11:0004:BITS=0
DO_read.PMC6_min_raw[1]
PLC1.C11:0005:BITS=0
DO_read.PMC6_max_raw[1]
PLC1.C11:0006:BITS=0
DO_read.Operating_hours[1]
PLC1.C11:0007:BITS=0
DO_read.Op_hours_above_max_mTemp[1]
PLC1.C11:0008:BITS=0
DO_read.Op_hours_above_max_opTemp[1]
PLC1.C11:0009:BITS=0
DO_read.Sensor_quality[1]
PLC1.C11:0010:BITS=0
DO_read.CP1_min[1]
PLC1.C11:0011:BITS=0
DO_read.CP1_max[1]
PLC1.C11:0012:BITS=0
DO_read.CP1_value_last_cal[1]
PLC1.C11:0013:BITS=0
DO_read.CP1_temp_value[1]
PLC1.C11:0014:BITS=0
DO_read.CP1_op_hour[1]
PLC1.C11:0015:BITS=0
DO_read.DO_at_CP1[1]
PLC1.C11:0016:BITS=0
DO_read.CP2_min[1]
PLC1.C11:0017:BITS=0
DO_read.CP2_max[1]
PLC1.C11:0018:BITS=0
DO_read.CP2_value_last_cal[1]
PLC1.C11:0019:BITS=0
DO_read.CP2_value_temp[1]
PLC1.C11:0020:BITS=0
DO_read.CP2_operating_hour[1]
PLC1.C11:0021:BITS=0
DO_read.CP1_pressure_value[1]
PLC1.C11:0022:BITS=0
DO_read.CP1_salinity_value[1]
PLC1.C11:0023:BITS=0
DO_read.CP2_pressure_value[1]
PLC1.C11:0024:BITS=0
DO_read.CP2_salinity_value[1]
PLC1.C11:0025:BITS=0
DO_read.DO_at_CP2[1]
PLC1.C11:0026:BITS=0
DO_read.CP6_min[1]
PLC1.C11:0027:BITS=0
DO_read.CP6_max[1]
PLC1.C11:0028:BITS=0
DO_read.CP6_DO_last_cal[1]
PLC1.C11:0029:BITS=0
DO_read.CP6_value_temp[1]
PLC1.C11:0030:BITS=0
DO_read.CP6_operating_hour[1]
PLC1.C11:0031:BITS=0
DO_read.CP6_pressure_value[1]
PLC1.C11:0032:BITS=0
DO_read.CP6_salinity_value[1]
PLC1.C11:0033:BITS=0
DO_read.Phi0[1]
PLC1.C11:0034:BITS=0
DO_read.Csv[1]
PLC1.C11:0035:BITS=0
DO_read.Reference_temp[1]
PLC1.C11:0036:BITS=0
DO_read.CP1_stored_oxygen_conc[1]
PLC1.C11:0037:BITS=0
DO_read.CP1_luminescence_shift[1]
PLC1.C11:0038:BITS=0
DO_read.CP1_cal_temp[1]
PLC1.C11:0039:BITS=0
DO_read.CP1_atmospheric_pressure[1]
PLC1.C11:0040:BITS=0
DO_read.CP2_stored_oxygen_conc[1]
PLC1.C11:0041:BITS=0
DO_read.CP2_luminescence_shift[1]
PLC1.C11:0042:BITS=0
DO_read.CP2_cal_temp[1]
PLC1.C11:0043:BITS=0
DO_read.CP2_atmospheric_pressure[1]
PLC1.C11:0044:BITS=0
DO_read.CP6_stored_oxygen_conc[1]
PLC1.C11:0045:BITS=0
DO_read.CP6_luminescence_shift[1]
PLC1.C11:0046:BITS=0
DO_read.CP6_cal_temp[1]
PLC1.C11:0047:BITS=0
DO_read.CP6_atmospheric_pressure[1]